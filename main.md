---
title: Intervalle de confiance pour une probabilité de dépassement de seuil dans un problème de dispersion atmosphérique
documentclass: scrartcl
author: Maéva Caillat, Sylvain Girard
<!-- date: 12 mars 2075 -->
link-citations: true
numbersections: true
lang: fr
---

\makeid

\tableofcontents{}

\clearpage

# Introduction

Ce rapport présente plusieurs méthodes d'estimation d'une probabilité de dépassement de seuil, dans une situation de dispersion atmosphérique.
L'enjeu est de taille car les résultats des simulations associées à cette estimation peuvent donner lieu à des prises de décision importantes.
L'estimation devra donc être suffisamment fiable et précise pour que les actions qui en découlent soient optimales.

# Exposé du problème

Dans cette partie, nous présentons le contexte de dispersion atmosphérique dans lequel nous nous plaçons, ainsi que le problème statistique qui se pose.
Puis, nous introduisons quatre méthodes d'estimation d'une probabilité de dépassement de seuil, issues de travaux de recherche sur l'estimation d'un paramètre binomial.

## Contexte : la dispersion atmosphérique

Soit X le vecteur aléatoire réel des sources d'incertitudes.

::: {.example}
**Exemple 1**. *Pour un instant t et un point de l'espace s donnés, par exemple : $$X(s,t) =\begin{bmatrix}
        X^{(1)}(s, t)\\
        X^{(2)}(s, t)\\
        X^{(3)}(s, t)\\
        X^{(4)}(s, t)
        \end{bmatrix}$$ avec $X^{(1)}(s, t)$ le terme source, $X^{(2)}(s, t)$ l'intensité des précipitations, $X^{(3)}(s, t)$ la vitesse du vent et $X^{(4)}(s, t)$ la direction du vent.*
:::

Soit $f$ une fonction mesurable appelée modèle numérique de transport. On souhaite obtenir des informations sur la distribution de $Y(s, t) = f(X(s, t))$, avec t le temps et s un point de l'espace. $Y(s, t)$ est le terme de propagation des incertitudes dans l'espace et le temps. Dans le cas de la dispersion atmosphérique, Y correspond au vecteur aléatoire perturbé en un point de l'espace et un instant donnés. On supprime la dimension temporelle de Y en prenant sa valeur à un instant fixé, en l'intégrant ou en prenant son maximum, par exemple. Désormais, on suppose que $Y(s) = f(X(s))$.

::: {.example}
**Exemple 2**. *Pour un point de l'espace donné, par exemple : $$Y(s) = \begin{bmatrix}
        Y^{(1)}(s)\\
        Y^{(2)}(s)
        \end{bmatrix}$$ avec $Y^{(1)}(s)$ la dose inhalation et $Y^{(2)}(s)$ la dose efficace, toutes deux en millisieverts (mSv).*
:::

Pour la suite du rapport, on suppose que $Y$ est un vecteur aléatoire 1D (soit une variable aléatoire). Par exemple, $Y$ représente la dose inhalation.

Soit $(\{0, 1 \}, \mathcal{P}(\{0, 1 \}), \{\mathcal{B}(p), p \in [0,1] \})$ un espace probabilisé par une loi de Bernoulli. Soit $\zeta \in \mathbb{R}^+$ la valeur seuil de la dose et soit $Z(s) = I_{\{Y(s) > \zeta\}}$ une variable aléatoire (v.a.) définie sur l'espace probabilisé ci-dessus. $Z(s) \sim \mathcal{B}(p(s))$ avec $p(s) = \mathbb{P}(Y(s) > \zeta)$. On a donc $\mathbb{E}(Z(s)) = p(s)$ et $\mathbb{V}(Z(s)) = p(s)(1-p(s))$.

Soit $\pi \in \mathbb{R}^+$ la valeur seuil de probabilité de dépassement de dose. On s'intéresse à l'événement $\{p(s) > \pi \}$ qui nous permet de prendre une décision en $s$. Par exemple, la décision à prendre peut être d'évacuer une zone géographique, d'imposer un confinement, de traiter avec de l'iode une partie de la population, etc.

La question qui se pose alors est la suivante : «Comment peut-on estimer $p(s)$ et quelle est la qualité souhaitée de l'estimation ?». La suite de cette partie est dédiée à la présentation de différentes méthodes d'estimation de $p$.

## Estimation ponctuelle

Un estimateur simple et très répandu dans la littérature est l'estimateur de la moyenne empirique.

Soit le modèle d'échantillonnage $(\mathcal{B}(p))_{p \in \Theta=[0,1]}$.

Soit $Z_1(s)$, \..., $Z_n(s)$ un n-échantillon de Z(s), c'est-à-dire $n$ v.a. indépendantes et identiquement distribuées (i.i.d.) selon la loi $\mathcal{B}(p(s))$. Elles admettent une variance finie $0<\sigma^2 = \mathbb{V}(Z_i) = p(1-p) < +\infty$ et sont de densité $f_p$ avec $\forall z \in \{ 0;1 \} f_p(z) = p^z (1-p)^{1-z}$.

On pose $S_n(s) = \sum_{i=1}^{n} Z_i(s) \sim \mathcal{B}(n, p(s))$. Soit $\hat{P}_n(s) = \frac{S_n}{n}$ la moyenne empirique de $Z(s)$. $\mathbb{E}(\hat{P}_n(s)) = p(s)$ et $\mathbb{V}(\hat{P}_n(s)) = \frac{p(s)(1-p(s))}{n}$. Cet estimateur possède de nombreuses qualités que nous développerons plus loin.

## Estimation par intervalle de confiance

$\hat{P}_n$ fournit une approximation ponctuelle de la quantité cible $p$ ; elle ne rend donc pas compte de l'incertitude de l'estimation. Une technique qui informe sur l'incertitude consiste à estimer le paramètre binomial $p$ grâce à un intervalle de confiance. L'estimation par intervalle d'une proportion binomiale donne lieu à une littérature prolifique (Clopper et Pearson 1934 [@clopper_pearson], Vollset 1993 [@vollset], Agresti et Coull 1998 [@agresti_coull], Newcombe 1998 [@newcombe], Brown et al. 2001 [@brown_cai_dasgupta], Pires et Amado 2008 [@pires_amado], pour n'en citer que quelques uns). Cette partie présente trois intervalles de confiance adaptés à la problématique étudiée.

### Intervalle standard de Wald

Historiquement, un des premiers intervalles de confiance à avoir été utilisé est l'intervalle de confiance asymptotique de Wald de niveau $1-\alpha$ pour $p$ (voire Laplace 1812 [@laplace]) : $$\hat{P}_n \pm z_{1-\frac{\alpha}{2}} \sqrt{\frac{\hat{P}_n(1-\hat{P}_n)}{n}}$$ avec $z_{1-\frac{\alpha}{2}}$ le quantile gaussien d'ordre $1-\frac{\alpha}{2}$.

### Intervalle de Wilson

L'intervalle de Wilson est basé sur l'inversion du test de score pour $p$. Il s'écrit : $$\left( \hat{P}_n + \frac{z_{1-\alpha/2}^2}{2n} \pm z_{1-\alpha/2} \sqrt{\frac{\hat{P}_n(1-\hat{P}_n)+\frac{z_{1-\alpha/2}^2}{4n}}{n}}\right) \frac{1}{1+ \frac{z_{1-\alpha/2}^2}{n}}$$

### Intervalle d'Agresti et Coull Add 4 (ou intervalle de Wald ajusté)

La méthode de Wald ajustée, suggérée par Agresti et Coull en 1998 («*the add two successes and two failures adjusted Wald interval*») s'écrit : $$\hat{\pi}_n \pm z_{1-\alpha/2} \sqrt{\frac{\hat{\pi}_n(1-\hat{\pi}_n)}{n+4}}$$ avec $\hat{\pi}_n = \frac{S_n+2}{n+4}$.

# Méthodes d'évaluation de la qualité de l'estimation

## Quelques rappels de statistique
Soit $\hat{p}$ un estimateur quelconque de $p$.

### Modes de convergence
Soit $(\hat{p}_n)_{n \geq 1}$ une suite d'estimateurs de $p$, avec $n$ la taille de l'échantillon.

- On dit que $(\hat{p}_n)_{n \geq 1}$ converge en $\mathbb{P}$-probabilité vers *p*, et on note $\hat{p}_n \overset{\mathbb{P}}{\underset{n \rightarrow +\infty}{\longrightarrow}} p$ si $\forall \epsilon > 0$ $\mathbb{P}(|\hat{p}_n - p| \geq \epsilon) \underset{n \rightarrow +\infty}{\longrightarrow} 0$

- On dit que $(\hat{p}_n)_{n \geq 1}$ converge $\mathbb{P}$-presque sûrement vers p, et on note $\hat{p}_n \overset{p.s.}{\underset{n \rightarrow +\infty}{\longrightarrow}} p$ si $\mathbb{P}(\{ \omega \in \Omega : \hat{p}_n(\omega) \underset{n \rightarrow +\infty}{\longrightarrow} p(\omega) \}) = 1$ avec $\Omega$ l'univers de l'expérience aléatoire.

La convergence presque sûre implique la convergence en probabilité.

### Le biais
Le biais d'un estimateur $\hat{p}$ de $p$ est la quantité déterministe : $$b(\hat{p}) = ||p-\mathbb{E}(\hat{p})||$$

L'estimateur est dit **sans biais** si $\mathbb{E}(\hat{p}) = p$ (biaisé sinon).

### Le risque quadratique
Le risque quadratique d'un estimateur de $p$ vaut : $$\mathcal{R}(\hat{p}) = \mathbb{E}[||\hat{p} - p||^2]$$

Avec la décomposition biais-variance du risque quadratique, on obtient : $$\mathcal{R}(\hat{p}) = \mathbb{V}(\hat{p}) + b(\hat{p})^2$$

Pour qu’un estimateur ait un risque quadratique faible, il faut que celui-ci admette conjointement un biais faible et une variance faible.

### Convergence et critères de performance d'un estimateur
Un estimateur est **asymptotiquement sans biais** si $\mathbb{E}(\hat{p}_n) {\underset{n \rightarrow +\infty}{\longrightarrow}} p$.

Un estimateur **converge en moyenne quadratique** si $\mathcal{R}(\hat{p}_n) {\underset{n \rightarrow +\infty}{\longrightarrow}} 0$.

Un estimateur est **consistant** (ou faiblement consistant) si $\hat{p}_n \overset{\mathbb{P}}{\underset{n \rightarrow +\infty}{\longrightarrow}} p$ (converge en $\mathbb{P}$-probabilité).

Un estimateur est **fortement consistant** si $\hat{p}_n \overset{p.s.}{\underset{n \rightarrow +\infty}{\longrightarrow}} p$ (converge $\mathbb{P}$-presque sûrement).

Soit $(v_n)_{n \geq 1}$ une suite de réels positifs qui tend vers l'infini. L'estimateur $\hat{p}_n$ est de **vitesse** $v_n$ s'il existe une loi non dégénérée (pas une loi de Dirac
en 0, ni une loi de Dirac à l’infini) telle que : $v_n (\hat{p}_n - p) \overset{\mathcal{L}}{\longrightarrow} l(p)$.
\newline Si la loi limite est une loi gaussienne, on dit alors que $\hat{p}_n$ est **asymptotiquement normal**.

Un estimateur $\hat{p}_n$ de $p$ est **préférable** à un estimateur $\tilde{p}_n$ si : $\mathcal{R}(\hat{p}_n) \leq \mathcal{R}(\tilde{p}_n)$.

### Information de Fisher
La **vraisemblance** pour le modèle $P_p = {(\mathcal{B}(p))_{p \in \Theta=[0,1]}}^{\otimes n}$ est la fonction définie sur $\Theta$ par : $$L_n(p) := L_n(Z_1, ..., Z_n, p) = \prod_{i=1}^n f_p(Z_i)$$

La fonction **log-vraisemblance** est : $l_n(p) = \ln(L_n(p))$.

Supposons que pour tout $(p, \alpha) \in \Theta^2$ on ait : $\mathbb{E}_p(|l_n(\alpha)|) < \infty$. La **divergence de Kullback-Leibler** entre les lois $P_p$ et $P_\alpha$ est définie par :
$$ KL(P_p, P_\alpha) = -\mathbb{E}_p[\ln (\frac{L_n(\alpha)}{L_n(p)})] = -\mathbb{E}_p[l_n(\alpha) - l_n(p)]$$
Il ne s’agit pas d’une distance, mais d’une dissimilarité entre mesures de probabilités dans le modèle statistique considéré.

Le modèle $(P_p)_{p \in [0,1]} = (L_n(., p) d\mu)_{p \in [0,1]}$ sur $\mathbb{R}^n$ est dit **régulier** si :

* pour $\mu$ presque tout $z \in \mathbb{R}^n$, avec $\mu$ une mesure de référence sur $\mathbb{R}^n$, l'application $p \mapsto L_n(z, p)$ est absolument continue sur $\Theta$ ;

* $\forall p_0 \in \Theta$, $\exists \Omega \subset \mathbb{R}^n$ tel que $\mu(\Omega) = 1$ et tel que $\forall z \in \Omega$, l’application $p \mapsto L_n' (z, p)$ est continue en $p_0$ ;

* $\forall p \in \Theta$, l'application : $z \mapsto \frac{L_n'(z, p)^2}{L_n(z, p)} I_{L_n(z, p) > 0}$ est intégrable sur $\mathbb{R}^n$ par rapport à la mesure de référence $\mu$ et d'intégrale : $$I_n(p) = \int_{\mathbb{R}^n} \frac{L_n'(z, p)^2}{L_n(z, p)} I_{L_n(z, p) > 0} \mu(dz)$$
continue sur $\Theta$.

La quantité $I_n(p)$ est alors appelée **information de Fisher** du modèle.
En particulier, si $\forall z \in \mathbb{R}^n$, $p \mapsto L_n(z, p)$ est $\mathcal{C}^1$ sur $\Theta$, les deux premiers points sont automatiquement vérifiés.

On a donc $I_n(p) = \mathbb{E}_p([l_n'(Z,p)]^2)$ où la variable aléatoire $l_n'(Z) = \frac{\partial}{\partial p} \ln L_n(Z, p)$ est appelée **fonction score**.

Si le modèle est régulier, l'information de Fisher est égale à la variance du score : $I_n(p) = \mathbb{V}_p(l_n'(Z,p))$.

Dans le cas i.i.d., on a : $I_n(p) = n I_1(p)$.

L’information de Fisher précise le pouvoir de discrimination du modèle entre deux valeurs proches du paramètre du modèle :

* Si l’information de Fisher $I_n(p)$ prend une valeur élevée, la nature des mesures de probabilité varie beaucoup au voisinage de $P_p$ (pour l’information de Kullback-Leibler).

* À l’inverse, une valeur faible de $I_n(p)$ traduit une faible variation de $KL(., P_p)$ au voisinage de $P_p$ et identifier $p$ apparaît alors comme un problème statistique plus difficile.


### Théorème de Cramer-Rao
Dans un modèle statistique régulier, soit $\hat{p}_n$ un estimateur de $p$ dont le risque quadratique est localement borné, de biais $b_{p,n} = \mathbb{E}_p(\hat{p}_n)-p$. Si $I_n(p) > 0$, on a alors la minoration du risque quadratique suivante :
$$ \mathcal{R}(\hat{p}_n) \geq b_{p,n}^2 + \frac{(1+ b_{p,n}')^2}{I_n(p)}$$
Si l'estimateur est sans biais, on obtient la borne de Cramer-Rao :
$$\mathcal{R}(\hat{p}_n) \geq \frac{1}{I_n(p)}$$

Un estimateur qui atteint cette borne est dit **efficace**.

## Méthode d'évaluation de la qualité d'une estimation ponctuelle

Pour évaluer la qualité d'une estimation ponctuelle, nous nous appuyons sur les critères suivants :

-   Le biais - un biais faible, voire nul, est souhaité.
-   Le risque quadratique - un risque quadratique faible est souhaité.
-   La consistance - une consistance forte est souhaitée.
-   La vitesse de convergence - une convergence rapide est souhaitée.
-   L'efficacité.

## Méthode d'évaluation de la qualité d'un intervalle de confiance

Soit $[L(S_n), U(S_n)]$ un intervalle de confiance pour $p$ et soit $1-\alpha \in [0,1]$ son niveau de confiance nominal. Il existe plusieurs critères d'évaluation d'un intervalle :

-   La probabilité de couverture réelle (*actual coverage probability*) est la probabilité qu'un intervalle contienne effectivement le paramètre $p$ : $$C_n(p) = \sum_{k=0}^{n} I(k,p) \binom{n}{k} p^k(1-p)^{n-k}$$ avec $I(k,p) = I_{p \in [L(S_n=k), U(S_n=k)]}$.\

    La probabilité de couverture moyenne est : $\mathbb{E}_n(C_n(p)) = \int_{0}^{1} C_n(p) dp$.

    La probabilité de couverture minimale est : $\underset{p \in [0,1]}{\inf} C_n(p)$.

    La probabilité de couverture nominale correspond au niveau de confiance nominal.

-   L'espérance de la largeur (ou *expected width*) est une bonne mesure de la performance de l'estimation par l'intervalle considéré. Elle s'écrit : $$\mathbb{E}_{n,p}(\text{length}(I_n)) = \sum_{k=0}^{n} (U(S_n=k)-L(S_n=k)) \binom{n}{k} p^k (1-p)^{n-k}$$
    La moyenne de l'espérance de largeur est : $\mathbb{E}_n(\text{length}(I_n)) = \int_{0}^{1} \mathbb{E}_{n,p}(\text{length}(I_n)) dp$


Deux classes d'intervalle s'opposent : les intervalles strictement conservatifs et les intervalles corrects en moyenne. Un intervalle conservatif a une probabilité de couverture réelle au moins égale au niveau de confiance nominal. Cela permet de prendre des précautions quant à l'estimation. Par exemple, un intervalle strictement conservatif de niveau de confiance nominal 95$\%$ peut avoir une probabilité de couverture réelle de 98$\%$ ou 99$\%$. Un intervalle correct en moyenne a une probabilité de couverture moyenne d'au moins $1-\alpha$. Par exemple, un intervalle de confiance nominale à 95$\%$ peut avoir une couverture de probabilité réelle inférieure à $95\%$, mais très proche de cette valeur. L'avantage des intervalles corrects en moyenne est qu'ils sont plus étroits que les intervalles strictement conservatifs, donc plus performants. Dans notre cas, nous excluons les intervalles strictement conservatifs pour nous concentrer sur des intervalles corrects en moyenne plus performants.

# Résultats et discussion

## Estimation ponctuelle

Dans un premier temps, évaluons la qualité de la moyenne empirique en tant qu'estimateur ponctuel. $\hat{P}_n$ :

1.  Est sans biais.

2.  A un risque quadratique faible et converge en moyenne quadratique.

3.  Est fortement consistant.

4.  Est asymptotiquement normal de vitesse de convergence $\sqrt{n}$.

5.  Atteint la borne de Cramer-Rao et est donc efficace.

Ainsi, la moyenne empirique est un bon estimateur ponctuel de $p$.

*Preuves.*

1.  $b(\hat{P}_n) = ||p - \mathbb{E}(\hat{P}_n)||=0$.

2.  Le risque quadratique est : $\mathcal{R}(\hat{P}_n) = \mathbb{E}[(\hat{P}_n - p)^2]$. Avec la décomposition biais-variance, on obtient : $\mathcal{R}(\hat{P}_n) = \mathbb{V}(\hat{P}_n) + b(\hat{P}_n)^2 = \mathbb{V}(\hat{P}_n) = \frac{p(1-p)}{n}$. Ainsi, $\hat{P}_n$ converge en moyenne quadratique car $\mathcal{R}(\hat{P}_n) \underset{n \rightarrow +\infty}{\longrightarrow} 0$.

3.  D'après la loi forte des grandes nombres, $\hat{P}_n \overset{p.s.}{\underset{n \rightarrow +\infty}{\longrightarrow}} p$, ce qui correspond à la définition de fortement consistant.

4.  D'après le théorème central limite (TCL) : $\sqrt{n} (\hat{P}_n-p) \overset{\mathcal{L}} \longrightarrow \mathcal{N}(0, \sigma^2)$.

5.  $\forall z \in \mathbb{R}^n$, $p \mapsto L_n(z, p) = \prod_{i=1}^n f_p(Z_i) = \prod_{i=1}^n p^{Z_i} (1-p)^{1-Z_i}$ est $\mathcal{C}^1$ sur $\Theta = [0,1]$.

$\forall p \in \Theta$, l'application : $z \mapsto \frac{L_n'(z, p)^2}{L_n(z, p)} I_{L_n(z, p) > 0}$ est intégrable sur $\mathbb{R}^n$ par rapport à la mesure de référence $\mu$ et d'intégrale : $I_n(p) = \int_{\mathbb{R}^n} \frac{L_n'(z, p)^2}{L_n(z, p)} I_{L_n(z, p) > 0} \mu(dz)$ continue sur $\Theta$. Le modèle est donc régulier. Comme on est dans le cas i.i.d., $I_n(p) = n I_1(p)$. On a : $I_1(p) = \mathbb{V}_p[l_1'(Z, p)]$, la variance de la fonction score.
$l_1'(p) = \frac{\partial}{\partial p} \ln L_1(Z,p) = \frac{Z-p}{p(1-p)}$. Donc $I_1(p) = \frac{1}{p(1-p)}$ et $I_n(p)=\frac{n}{p(1-p)}$. Ainsi, le risque quadratique est égal à la borne de Cramer-Rao : $\mathcal{R}(\hat{P}_n) = \frac{1}{I_n(p)}$ et donc $\hat{P}_n$ est efficace.

## Estimation par intervalle de confiance

Dans cette partie, nous comparons les trois intervalles de confiance définis plus haut sur des échantillons de taille $n$ d'un variable aléatoire qui suit une loi de Bernoulli, pour différentes valeurs de $n$ et différentes valeurs de confiance nominal $\alpha$.

### Comparaison des probabilités de couverture des trois intervalles

Les figures @fig:coverage_proba_95_50 et @fig:coverage_proba_95_500 montrent que la probabilité de couverture réelle de la méthode de Wald est bien moins bonne que celles de score et d'Add 4, pour des valeurs de $p$ proche de 0 (ou 1 donc). Par ailleurs, la méthode Add 4 est légèrement plus conservative que celle de Score pour des valeurs de $p$ proches de 0 ou 1. En revanche, quand $p$ est éloigné des extrêmes, les trois intervalles semblent converger vers une même probabilité de couverture réelle. Par exemple, pour $n=50$ et $\alpha=0,05$, pour $p$ compris entre $0,45$ et $0,55$, les trois méthodes ont à-peu-près la même couverture réelle. Pour $n=500$ et $\alpha=0,05$, pour $p$ compris entre $0,25$ et $0,75$, les trois méthodes ont à-peu-près la même couverture réelle.

![Probabilité de couverture en fonction de $p$ pour $n=50$ et $\alpha=0,05$](figure/coverage_proba_95_50.png){#fig:coverage_proba_95_50}

![Probabilité de couverture en fonction de $p$ pour $n=500$ et $\alpha=0,05$](figure/coverage_proba_95_500.png){#fig:coverage_proba_95_500}

La figure @fig:mean_coverage_95 montre que la probabilité de couverture moyenne de la méthode de Wald est bien moins bonne que celles de Score et d'Add 4, quelle que soit la taille de l'échantillon $n$. Par exemple, pour $n = 10$, elle est de $77 \%$ pour Wald, $95,5 \%$ pour Score et $96,5 \%$ pour Add 4. Cette figure montre aussi que la méthode de Score est la méthode la plus performante en ce qui concerne la couverture moyenne, quelle que soit la valeur de $n$, même si Add 4 et la méthode de Score semblent converger vers une même couverture moyenne quand n est grand.

![Probabilité de couverture moyenne en fonction de $n$ pour $\alpha=0,05$](figure/mean_coverage_95.png){#fig:mean_coverage_95}

La figure @fig:min_coverage_95 montre que la probabilité de couverture minimum de la méthode de Wald est bien moins bonne que celles de Score et d'Add 4, quelle que soit la taille de l'échantillon $n$. Par exemple, pour $n = 1000$, elle est de $60 \%$ pour Wald, $94,7 \%$ pour Score et $95 \%$ pour Add 4. Cette figure montre aussi que la méthode Add 4 est la méthode la plus performante en ce qui concerne la couverture minimum, quelle que soit la valeur de n, même si Add 4 et et Score semblent converger vers une même couverture minimum quand n est grand.

![Probabilité de couverture minimum en fonction de $n$ pour $\alpha=0,05$](figure/min_coverage_95.png){#fig:min_coverage_95}

### Comparaison des largeurs moyennes des trois intervalles

La figure @fig:expected_width_95_15 affiche un intervalle de Wald plus étroit que les intervalles de Score et Add 4 pour des valeurs de $p$ proches de 0 ou 1. Pour des valeurs de $p$ non-extrêmes, Score a l'intervalle le plus étroit, puis Add4, puis Wald.

![Largeur moyenne en fonction de $p$ pour $n=15$ et $\alpha=0,05$](figure/expected_width_95_15.png){#fig:expected_width_95_15}

La figure @fig:expected_width_99_15 montre que, plus le niveau de confiance est exigeant, plus les intervalles s'élargissent, quelle que soit la valeur de $p$.

![Largeur moyenne en fonction de $p$ pour $n=15$ et $\alpha=0,01$](figure/expected_width_99_15.png){#fig:expected_width_99_15}

Les figures @fig:expected_width_95_50 et @fig:expected_width_95_200 indiquent que plus $n$ est grand, plus les intervalles semblent se resserrer et se rapprocher de la même performance en matière de largeur d'intervalle moyenne. Par exemple, pour $\alpha = 0,05$, à partir de $n=200$, les trois intervalles ont la même largeur moyenne, quelle que soit la valeur de $p$.

![Largeur moyenne en fonction de $p$ pour $n=50$ et $\alpha=0,05$](figure/expected_width_95_50.png){#fig:expected_width_95_50}

![Largeur moyenne en fonction de $p$ pour $n=200$ et $\alpha=0,05$](figure/expected_width_95_200.png){#fig:expected_width_95_200}

# Recommandations

Suite à cette étude comparative, nous recommandons d'estimer la probabilité de dépassement de seuil $p$ à l'aide d'un intervalle de confiance, afin de rendre compte des incertitudes liées à l'estimation.

La méthode de Wald est à exclure, quelle que soit la taille de l'échantillon $n$ et quelle que soit la valeur de $p$. En effet, pour des tailles d'échantillons faibles, Wald a une probabilité de couverture très en-dessous de sa valeur nominale. Pour de grands échantillons (par exemple, $n=500$), Wald a une probabilité de couverture proche de sa valeur nominale pour $p$ éloigné de 0 ou 1, avec la même largeur d'intervalle moyenne que les méthodes de Score ou Add 4, mais avec une probabilité de couverture moyenne moins bonne que celles de Score ou Add 4.

Maintenant que la méthode de Wald est écartée, quel intervalle de confiance choisir entre celui de Score et celui d'Add 4 ? Tout dépend du degré de conservatisme désiré. En effet, si le décideur veut un intervalle à la couverture réelle au moins égale à sa couverture nominale, alors nous recommandons d'utiliser l'intervalle d'Add 4. En effet, quelle que soit la taille d'échantillon et le niveau de confiance, Add 4 a une probabilité de couverture minimum plus proche de la couverture nominale que Score. Au contraire, si ce qui importe au décideur est d'avoir un intervalle peu conservatif très proche de sa valeur de couverture nominale en moyenne, alors nous recommandons l'intervalle de Score. En effet, quelle que soit la taille d'échantillon et le niveau de confiance, l'intervalle de Score est plus proche en moyenne de sa couverture nominale, et a une largeur moyenne inférieure ou égale à celle d'Add 4.

Par ailleurs, pour $\alpha = 0,05$ et $n > 200$, les méthodes de Score et d'Add 4 ont des performances très proches, tant pour leur largeur d'intervalle moyenne que pour leurs probabilités de couverture moyenne ou minimale, sauf pour $p$ très proche de 0 ou 1 où Add 4 est plus performante.


# Application à un cas pratique

Dans cette partie, nous allons appliquer la méthode Add 4 à un cas concret de la prise en compte des incertitudes de dispersion de radionucléïdes dans l'atmosphère à la suite d'un accident nucléaire.

## Cas d'étude

On se place dans le cas d'une perte du refroidissement ajouté à une défaillance des systèmes de sauvegardes, qui mène à une fusion du coeur du réacteur. Il y a alors une formation de corium et une progression vers le fond de la cuve, pour finalement la percer lorsque la pression de la cuve dépasse la pression de l'enceinte. Il y a alors une éjection et fragmentation du corium et de la vapeur d'eau, ainsi qu'un échange thermique et une oxydation qui mène à la production d'hydrogène et la combustion de celui-ci. L'échauffement créé et la pressurisation de l'atmosphère endommage et fait perdre l'intégrité du confinement, pour ensuite causer un rejet massif et non contrôlé de particules radioactives hors de l'enceinte, directement dans l'atmosphère.

C'est un scénario accidentel très rare, mais qui mène le plus souvent à des conséquences très graves sur les populations environnantes.

Dans cette étude, on suppose que ce genre d'événement se produit à la centrale nucléaire de Dampierre, dans le bâtiment réacteur 1.

## Données

### Terme source

Le terme source de référence utilisé pour cette étude est un terme source caractéristique des accidents de type DCH. Il a une cinétique particulière avec deux gros rejets principaux : un premier au début de la fusion du coeur et un deuxième au moment où le confinement est endommagé (cf. figure {@fig:terme_source}).

![Cinétique cumulée du terme source DCH de référence. Chaque courbe représente l'évolution d'un isoélément du terme source.](figure/ts.png){#fig:terme_source}

Nous considérons plusieurs types d'incertitudes pour ce genre d'événement :

 - **Hauteur de rejet** : La hauteur du rejet est tiré aléatoirement entre $0$ et $100$ m.
 - **Amplitude du premier rejet** : L'amplitude du premier rejet est perturbée d'un facteur multiplicatif tiré aléatoirement entre $2$ et $1/2$ et centré en $1$.
 - **Amplitude du deuxième rejet** : L'amplitude du deuxième rejet est perturbée d'un facteur multiplicatif tiré aléatoirement entre $2$ et $1/2$ et centré en $1$.
 - **Timing du premier rejet** : Le timing du premier rejet est perturbé en temps par un facteur additif tiré aléatoirement entre $-3$ et $3$ heures.
 - **Temps qui sépare les deux rejets** : Le timing du deuxième rejet est tiré aléatoirement selon une loi log-normale de moyenne $20$ heures et d'écart-type $30$ heures, avec un minimum imposé à $3$ heures, afin de prendre en compte des cinétiques différentes sur le timing du DCH, qui peuvent même ne jamais se produire (lorsque leur timing dépassent la durée maximale du terme source).

 La figure {@fig:terme_source_perturbe} illustre ces perturbations sur la cinétique de rejet du Césium 137.

![Perturbation du terme source du Césium 137 en prenant en compte les incertitudes sur le timing et l'amplitude des rejets.](figure/ts_perturbe_180.png){#fig:terme_source_perturbe}


### Données météorologiques

La météo utilisée est la météo ARPEGE, sur une zone de plusieurs kilomètres autour de la centrale de Dampierre-en-Yvelines. Dans cette étude, on ne prend pas en compte l'incertitude météo, en perturbant pas la météo. L'utilisation d'un ensemble météo sera envisagé plus tard dans la suite du projet.

### Vitesse de dépôt

Dans cette étude, les vitesses de dépôt sec et de dépôt humide sont supposées incertaines et sont caractérisées par :

 - **Vitesse de dépôt sec pour les aérosols** : tirée aléatoirement entre $5 \times 10^{-4}$ et $5 \times 10^{-3}$.
 - **Vitesse de dépôt sec pour l'iode i2** : tirée aléatoirement entre $2 \times 10^{-3}$ et $2 \times 10^{-2}$.
 - **Coefficient de lessivage** : tiré aléatoirement entre $1 \times 10^{-5}$ et $1 \times 10^{-4}$.

## Résultats

100 simulations pX sont effectuées pour chaque modèle de stabilité de l'air (Doury, Pasquill, Similitude), sur un domaine d'étude circulaire de 100 km autour de la source. On a donc un ensemble de 300 résultats de dose efficace et dose inhalation, pour appliquer la méthode Add 4.

En définissant un seuil de dose, une probabilité de dépassement et un niveau de confiance, l'intervalle Add 4 nous définit un intervalle de confiance qui nous permet de définir 3 zones de décision :

 - Une zone où on est certain de devoir prendre une décision (en rouge sur la figure {@fig:bokeh}), où notre estimateur se situe au-dessus des deux bornes de notre intervalle de confiance.
 - Une zone où on est certain de ne devoir prendre aucune décision (en blan sur la figure {@fig:bokeh}), où notre estimateur se situe en dessous des deux bornes de notre intervalle de confiance.
 - Une zone il y a de l'incertitude sur la décision (en gris sur la figure {@fig:bokeh}), où notre estimateur se situe entre les deux bornes de notre intervalle de confiance.

Nous faisons apparaitre ces trois zones sur la figure {@fig:bokeh}, qui donne un exemple d'interface graphique possible pour de telle estimations. Notons qu'ici il est possible de choisir le modèle de stabilité de l'air que l'on souhaite pour observer les résultats, mais il aurait été possible d'inclure les trois modèles sur le résultat, en considérant les 300 simulations d'un coup au lieu de les considérer par groupe de 100. Par ailleurs, nous avons rendu possible le choix de la date à laquelle on se place. Bien que ce genre de décision se fasse en général plusieurs heures après le début des rejets, il est intéressant de pouvoir observer l'influence du timing dans la décision.

![Exemple d'interface graphique possible pour l'estimation des zones de décisions en fonction de différents paramètres permettant de définir l'estimation que l'on souhaite observer.](figure/bokeh.png){#fig:bokeh}

Si ce genre de cartes est utilisé en crise, il est alors possible d'en tirer des zones de décision, tout en observant l'influence de chaque paramètre (seuil de dose, seuil de dépassement de probabilité, pourcentage de confiance) sur le résultat grâce à l'interactivité des figures. 

Dans la pratique, la zone grise doit être prise en compte dans la décision finale, car elle définit une zone où le seuil réel de dose peut probablement être dépassé.

# Conclusion

La méthode Add 4 peut être utilisée en pratique pour estimer un intervalle de confiance qui encadre une probabilité de dépassement de seuil de dose à la suite d'un accident nucléaire. En comparaison des autres méthodes similaires, c'est celle qui donne de meilleurs résultats pour les tailles d'échantillons considérés en crise. Par ailleurs, c'est l'estimation la plus conservatrice, et permet donc d'avoir une meilleure confiance dans le résultat.

Avec cette méthode, il est possible d'utiliser l'information de l'intervalle de confiance pour déterminer des zones d'actions (évacuation, mise à l'abri, prise d'iode) avec trois niveaux de décision (décision sûre, décision probable, non-décision), en fonction de si on se situe au-dessus, dans ou en dessous de cet intervalle.

Par ailleurs, contrairement à des méthodes Bayesiennes qui pourraient être plus précises, la méthode Add 4 ne demande aucun effort de calcul supplémentaire, ce qui lui permet donc d'être utilisée très facilement dans un contexte opérationel.


# Références
